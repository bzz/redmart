//
//  AppDelegate.swift
//  RedmartProductParser
//
//  Created by Mikhail Baynov on 02/06/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

let postman = Postman()
let model = Model()



@UIApplicationMain
open class AppDelegate: UIResponder, UIApplicationDelegate {
   
   open var window: UIWindow?
   
   
   open func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      
      window = UIWindow(frame: UIScreen.main.bounds)
      window!.backgroundColor = UIColor.white
      window?.rootViewController = StockVC()
      window?.makeKeyAndVisible()
      model.imageCache = model.loadDic("imageCache")
      return true
   }
   
   open func alert(_ title:String, message: String) {
      let alert = UIAlertController(title: title, message:message, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
      self.window?.rootViewController!.present(alert, animated: true){}
   }
   
   
   open func applicationDidEnterBackground(_ application: UIApplication) {
      model.saveDic(model.imageCache, userDefaultsKey: "imageCache")
   }
   
}

