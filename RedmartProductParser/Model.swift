//
//  Model.swift
//  RedmartProductParser
//
//  Created by Mikhail Baynov on 02/06/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//
import UIKit


open class Model {
   
   var productsArray = NSMutableArray()
   var imageCache = NSMutableDictionary()
   var stockURL = "https://api.redmart.com/v1.5.6/catalog/search?theme=all-sales&pageSize=30&page=0"
   var currentStockPage = 0 {
      didSet {
         stockURL = "https://api.redmart.com/v1.5.6/catalog/search?theme=all-sales&pageSize=30&page=" + String(currentStockPage)
      }
   }
   
   
   
   
   func saveDic(_ dic: AnyObject, userDefaultsKey : String) {
      print("SAVING " + userDefaultsKey)
      do {
         //         print(history.description)
         let data = try PropertyListSerialization.data(fromPropertyList: dic, format:PropertyListSerialization.PropertyListFormat.xml, options: 0)
         UserDefaults.standard.set(data, forKey:userDefaultsKey)
         UserDefaults.standard.synchronize()
      } catch {
         print("Error reading plist: \(error)")
      }
   }
   
   func loadDic(_ userDefaultsKey : String) -> NSMutableDictionary {
      print("LOAD " + userDefaultsKey)
      var out : NSMutableDictionary?
      var format = PropertyListSerialization.PropertyListFormat.xml
      if let plistXML = UserDefaults.standard.object(forKey: userDefaultsKey) as? Data {
         do {
            out = try PropertyListSerialization.propertyList(from: plistXML, options: .mutableContainersAndLeaves, format: &format) as? NSMutableDictionary
         }
         catch {
            print("Error reading plist: \(error), format: \(format)")
         }
      }
      if out != nil {
         return out!
      } else {
         return NSMutableDictionary()
      }
   }

}
