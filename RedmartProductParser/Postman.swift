//
//  Postman.swift
//
//
//  Created by Mikhail Baynov on 1/26/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//
import UIKit



open class Postman:NSObject, URLSessionDelegate, UIAlertViewDelegate {

   let session = URLSession.shared
   var datastring = String()
   var Q : DispatchQueue?
   
   override init() {
      super.init()
      Q = DispatchQueue(label: "postman.background.q", attributes: []);
   }
   
   func getStockData() {
      DispatchQueue.global(qos: .default).async {
         
         let url = URL(string: model.stockURL)
         let request = NSMutableURLRequest(url: url!)
         request.httpMethod = "GET"
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.addValue("application/json", forHTTPHeaderField: "Accept")
         

         let task = self.session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil else {
               print("no data found: \(String(describing: error))")
               return
            }
            do {
               self.datastring = NSString(data:data!, encoding:String.Encoding.utf8.rawValue)! as String
               print("GOT DATA FROM SERVER: \(self.datastring)")
               
               if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                  print(json.description)                  
                  DispatchQueue.main.async {
                                         
                     model.productsArray.removeAllObjects()
                     guard let products = json["products"] as? NSArray else { return }
                     for c in products {
                        model.productsArray.add(c)
                     }
                     
                     Notifier.post(.gotStockData)
                  }
                  
               } else {
                  let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                  print("Error could not parse JSON: \(String(describing: jsonStr))")
               }
            } catch let error {
               print(error)
               let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
               print("Error could not parse JSON: '\(String(describing: jsonStr))'")
            }
         }) 
         task.resume()
         
         
      }
   }
   
   
   
   
   
}

