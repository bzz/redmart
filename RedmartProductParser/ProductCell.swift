//
//  Item.swift
//  RedmartProductParser
//
//  Created by Mikhail Baynov on 02/06/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit



class ProductCell: UITableViewCell {
   var picture = UIImageView()
   var titleLabel = UILabel()
   var priceLabel = UILabel()
   
   
   override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      
      self.backgroundColor = UIColor(hex: 0xf6f6f7)
      self.selectionStyle = .none
   }
   
   required init(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)!
      print("init(coder:) has not been implemented")
   }
}
