import UIKit



class StockVC: UIViewController {
   let screenHeight = UIScreen.main.bounds.height
   let screenWidth = UIScreen.main.bounds.width
   
   var feedArray = NSMutableArray()
   var tableView = UITableView()
   let btnHeight :CGFloat = 49 * UIScreen.main.bounds.width / 320
   let btnWidth  :CGFloat = 64 * UIScreen.main.bounds.width / 320
   
   var bottomPanel = UIView()
   

   
   override func viewDidLoad() {
      feedArray = model.productsArray
      
      bottomPanel = UIView(frame: CGRect(x: 0, y: screenHeight-btnHeight, width: screenWidth, height: btnHeight))
      bottomPanel.backgroundColor = UIColor(hex: 0xffffff)
      view.addSubview(bottomPanel);
      
      
      let leftButton = UIButton(frame: CGRect(x: 0, y: 0, width: btnWidth, height: btnHeight))
      leftButton.setBackgroundImage(UIImage(named: "arrow_left"), for: UIControl.State())
      leftButton.adjustsImageWhenHighlighted = false
      leftButton.addTarget(self, action:#selector(StockVC.leftButtonPressed(_:)), for: UIControl.Event.touchDown)
      bottomPanel.addSubview(leftButton)

      let rightButton = UIButton(frame: CGRect(x: btnWidth*4, y: 0, width: btnWidth, height: btnHeight))
      rightButton.setBackgroundImage(UIImage(named: "arrow_right"), for: UIControl.State())
      rightButton.adjustsImageWhenHighlighted = false
      rightButton.addTarget(self, action:#selector(StockVC.rightButtonPressed(_:)), for: UIControl.Event.touchDown)
      bottomPanel.addSubview(rightButton)

      
      
      
      
      
      
      tableView = UITableView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight-btnHeight))
      tableView.delegate = self
      tableView.dataSource = self
      tableView.register(ProductCell.self, forCellReuseIdentifier: String(describing: ProductCell()))
      tableView.backgroundColor = UIColor.clear
      
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableView.automaticDimension;
      
      
      let headerView = UIView(frame: CGRect(x:0, y:0, width:view.frame.width, height:130))
      
      let headerContentView = UILabel(frame: headerView.bounds)
      headerContentView.text = "Redmart stock"
      headerContentView.textColor = UIColor.red
      headerContentView.textAlignment = .center
      headerContentView.font = UIFont(name: "HelveticaNeue-Bold", size: 24)

      
      
      headerView.addSubview(headerContentView)
      tableView.tableHeaderView = headerView
      
      
      tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: -10, right: 0);
      
     
      view.addSubview(tableView)
      
      Notifier.addObserver(self, event: .gotStockData)
      
      postman.getStockData()
   }
   
   
   deinit {
      Notifier.removeObserver(self, event: .gotStockData)
   }

   func gotStockData(_ nofif: Notification) {
      tableView.reloadData()
   }
   
   @objc func leftButtonPressed(_ button:UIButton) {
      print("leftButtonPressed")
      if model.currentStockPage > 0 {
         model.currentStockPage -= 1
         postman.getStockData()
      }
   }
   @objc func rightButtonPressed(_ button:UIButton) {
      model.currentStockPage += 1
      postman.getStockData()
   }

}


extension StockVC: UITableViewDelegate, UITableViewDataSource {
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return feedArray.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductCell()), for: indexPath) as! ProductCell
      
      let dic = feedArray[indexPath.row] as! NSDictionary

      
      
      
      
      
      let pic = getImg(dic)
      if pic != nil {
         cell.picture = UIImageView(image: pic)
      
      
      } else {
         cell.picture = UIImageView()

         guard let img = dic["img"] as? NSDictionary else { return cell }
         guard let link = img["name"] as? String else { return cell }
         let url = "http://media.redmart.com/newmedia/200p" + link

         print(url)
         DispatchQueue.global(qos: .default).async {
            if let d = try? Data(contentsOf: URL(string:url)!) {
               model.imageCache[url] = d
               DispatchQueue.main.async {
                  tableView.reloadRows(at: [indexPath], with: .automatic)
               }
            }
         }
      }
      cell.picture.frame.origin.y = 30
      cell.addSubview(cell.picture)

      
      if let title = dic["title"] as? String {
         cell.titleLabel = UILabel(frame:CGRect(x:20, y:7, width:UIScreen.main.bounds.width - 85, height:20))
         cell.titleLabel.numberOfLines = 3
         cell.titleLabel.text = title
         cell.titleLabel.textColor = UIColor(hex: 0x000000)
         cell.titleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 13)
         cell.addSubview(cell.titleLabel)
      }
      
      if let pricing = dic["pricing"] as? NSDictionary {
         if let price = pricing["price"] as? Float {
            cell.priceLabel = UILabel(frame:CGRect(x:UIScreen.main.bounds.width - 150, y:30, width:140, height:20))
            cell.priceLabel.text = "Price: " + (NSString(format:"%.02f", price) as String)
            cell.priceLabel.textColor = UIColor(hex: 0x000000)
            cell.priceLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
            cell.addSubview(cell.priceLabel)
         }
      }
      

      
      return cell
   }
   
   func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
      let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ProductCell()), for: indexPath) as! ProductCell
      cell.picture.isHidden = true
      cell.titleLabel.isHidden = true
      cell.priceLabel.isHidden = true
   }
   
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      let dic = feedArray[indexPath.row] as! NSDictionary
      if let pic = getImg(dic) {
         return pic.size.height + 30
      } else {
         return 88
      }
   }
   
   
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
      let offsetY = scrollView.contentOffset.y;
      let headerContentView = self.tableView.tableHeaderView!.subviews[0];
      if offsetY < -10 {
         headerContentView.transform = CGAffineTransform(translationX: 0, y: offsetY); //MIN(offsetY, 0)
      } else {
         headerContentView.transform = CGAffineTransform(translationX: 0, y: -10);
      }
   }
   
   
   
   func getImg(_ dic : NSDictionary) -> UIImage? {
      guard let img = dic["img"] as? NSDictionary else { return nil }
      guard let link = img["name"] as? String else { return nil }
      let url = "http://media.redmart.com/newmedia/200p" + link
      
      if let im = model.imageCache[url] as? Data {
         return UIImage(data:im)
      } else {
         return nil
      }
   }



}




