import Foundation


public enum NotifierEvent : String {
   case gotStockData = "gotStockData"
   
}



public final class Notifier { 

   public static func post(_ event: NotifierEvent) {
      print("NOTIFIER: \(event.rawValue)")
      NotificationCenter.default.post(name: Notification.Name(rawValue: event.rawValue), object:self)
   }   
   public static func post(_ event: NotifierEvent, userInfo: [AnyHashable: Any]) {
      print("NOTIFIER: \(event.rawValue) \(userInfo.description)")
      NotificationCenter.default.post(name: Notification.Name(rawValue: event.rawValue), object:self, userInfo:userInfo)
   }
   
   
   public static func addObserver(_ observer: AnyObject, event : NotifierEvent) {
      NotificationCenter.default.addObserver(observer, selector: Selector(event.rawValue + ":"), name: NSNotification.Name(rawValue: event.rawValue), object :nil)
   }
   
   public static func removeObserver(_ observer: AnyObject, event : NotifierEvent) {
      NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: event.rawValue), object: nil)
   }

}
